package huyencool.com.music2;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import huyencool.com.music2.model.BaiHat;

public class MediaPlaybackService extends Service implements MediaPlayer.OnCompletionListener {
    MediaPlayer mediaPlayer;

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }
    @Override
    public void onCreate() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String song = intent.getStringExtra("song");//nhan nhac tu AllSongs sang
        String action = intent.getStringExtra("action");
        if (song != null && action != null && !action.equals("") && !song.equals("")) {
            Uri uri = Uri.parse(song);
            switch (action) {
                case "new":
                    if (mediaPlayer != null){
                        mediaPlayer.stop();
                        mediaPlayer.release();
                    }
                    mediaPlayer = MediaPlayer.create(this, uri);// raw/s.mp3 // khoi tao de bat dau phat bai co ID(tieng nhac)la song
                    mediaPlayer.setOnCompletionListener(this);
                    mediaPlayer.start();
                    break;
                case "pause":
                    if (mediaPlayer != null)
                    mediaPlayer.pause();
                    break;
                case "play":
                    if (mediaPlayer != null)
                    mediaPlayer.start();
                    break;

                case "goto":
                    int timeTo = intent.getIntExtra("timeTo",0);
                    if (mediaPlayer != null)
                    mediaPlayer.seekTo(timeTo);
                    break;
                default:            // goi get time
                    if (mediaPlayer != null)
                        sendBroadcast(mediaPlayer.getCurrentPosition(), mediaPlayer.isPlaying());
                    break;
            }

        }


        return START_STICKY;
    }

    private void sendBroadcast (int currentTime, boolean isPlaying){
        Intent intent = new Intent ("message"); //put the same message as in the filter you used in the activity when registering the receiver
        intent.putExtra("currentTime", currentTime + "");
        intent.putExtra("isPlaying",isPlaying );

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
    public void onDestroy() {
        if (mediaPlayer != null){
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
        }

    }

    public void onCompletion(MediaPlayer _mediaPlayer) {
        stopSelf();
    }


}