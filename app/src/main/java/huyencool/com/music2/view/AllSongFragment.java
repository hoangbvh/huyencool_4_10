
package huyencool.com.music2.view;



        import android.Manifest;
        import android.app.Activity;
        import android.app.AlertDialog;
        import android.content.ContentValues;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.SharedPreferences;
        import android.content.pm.PackageManager;
        import android.content.res.Configuration;
        import android.database.Cursor;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Point;
        import android.media.MediaMetadataRetriever;
        import android.net.Uri;
        import android.os.Build;
        import android.os.Bundle;

        import androidx.annotation.NonNull;
        import androidx.annotation.Nullable;
        import androidx.core.app.ActivityCompat;
        import androidx.core.content.ContextCompat;
        import androidx.fragment.app.Fragment;
        import androidx.recyclerview.widget.LinearLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;

        import android.provider.MediaStore;
        import android.util.DisplayMetrics;
        import android.util.Log;
        import android.view.Display;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.animation.OvershootInterpolator;
        import android.widget.FrameLayout;
        import android.widget.Toast;

        import java.util.ArrayList;
        import java.util.concurrent.TimeUnit;


        import huyencool.com.music2.Library;
        import huyencool.com.music2.MyNotification;
        import huyencool.com.music2.R;
        import huyencool.com.music2.controller.LayoutController;
        import huyencool.com.music2.model.BaiHat;
        import huyencool.com.music2.model.FavoriteSongsProvider;
        import huyencool.com.music2.view.new_interface.AllSongView;
        import huyencool.com.music2.view.new_interface.ListMusicListener;
        import huyencool.com.music2.view.new_interface.ViewPlayerListener;

        import static android.content.Context.MODE_PRIVATE;

public class AllSongFragment extends Fragment implements ViewPlayerListener {
    private FrameLayout fragmentMediaPlaybackContainer;
    private ArrayList<BaiHat> mBaiHat = new ArrayList<>();
    private RecyclerViewAdapter listMusicRecycleViewAdapter = new RecyclerViewAdapter();

    private int positionActive = -1;

    //screen size
    private int screenHeight = 0;

    private int count = 0;

    //new variable
    public AllSongFragment() {

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (positionActive != -1) {
            outState.putSerializable("baihat", mBaiHat.get(positionActive));
        }
        outState.putInt("pos", positionActive);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    RecyclerView listMusicRecycleView;
    private ListMusicListener listMusicListener;
    private AllSongView allSongView;

    public void setAllSongView(AllSongView allSongView) {
        this.allSongView = allSongView;
    }

    public void setListMusicListener(ListMusicListener listMusicListener) {
        this.listMusicListener = listMusicListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_all_song, container, false);
        listMusicRecycleView = view.findViewById(R.id.listMusicRecycleView);
        fragmentMediaPlaybackContainer = view.findViewById(R.id.fragmentMediaPlaybackContainer);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;

        fragmentMediaPlaybackContainer.setTranslationY(screenHeight);
        if (savedInstanceState != null) {
            if (positionActive == -1)
            positionActive = savedInstanceState.getInt("pos");
            //sau khi tu chieu ngang -> chieuf dung thì hiển thị lại view chơi nhạc ở dưới màn hình
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fragmentMediaPlaybackContainer.animate().setInterpolator(new OvershootInterpolator(1)).translationY(screenHeight - LayoutController.dpToPx(60 + 55 + getStatusBarHeight()));      // giá trị viewTranslationY sau khi đã trừ statusbar va toolbar
                    }
                });
            }
        }


        if (checkPermissionREAD_EXTERNAL_STORAGE(getContext())) {
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String selection = MediaStore.Audio.Media.IS_MUSIC + "!=0";
            Cursor cursor = getActivity().getContentResolver().query(uri, null, selection, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    int stt = 1;
                    do {
                        String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                        String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                        Long times = Long.parseLong(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)));
                        String url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                        String id = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media._ID));

                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                        retriever.setDataSource(url);
                        byte[] coverBytes = retriever.getEmbeddedPicture();
                        Bitmap songCover = null;
                        if (coverBytes!=null) //se l'array di byte non è vuoto, crea una bitmap
                            songCover = BitmapFactory.decodeByteArray(coverBytes, 0, coverBytes.length);
                        String musicImage = "";
                        if (songCover != null) {

                            musicImage = Library.BitMapToString(songCover);
                        }


                        BaiHat b = new BaiHat(stt + "", name, artist, times, url, musicImage,id);

                        mBaiHat.add(b);
                        stt++;
                    } while (cursor.moveToNext());

                }

                cursor.close();
            }

        }
        ArrayList<BaiHat> allMusic = mBaiHat;             // lưu lai tất cả bài hát vào biến này để có thể chuyển từ danh sách yêu thích sang tất cả bài hát mà ko cần gọi lại hàm lấy bài hát
        allSongView.setViewPlayerListener(this);
        listMusicListener.setListMusic(allMusic);

        final int viewTranslationY = screenHeight - LayoutController.dpToPx(60 + 55 + getStatusBarHeight());   // 60 là chiều cao của phần hiển thị (ảnh tên, nút pau/play ...viewPlayer ở dưới), 55 là chiều cao của toolbar
        listMusicRecycleViewAdapter = new RecyclerViewAdapter(mBaiHat, positionActive, new OnClickRecyclerViewItem() {
            @Override
            public void onItemClickListener(int position) {
                String selection = " id_provider =" + mBaiHat.get(position).getmStt();
                Cursor c = getActivity().managedQuery(FavoriteSongsProvider.CONTENT_URI, null, selection, null, null);
                if (c.moveToFirst()) {
                    do {
                       //Log.d("ID",c.getString(c.getColumnIndex("id_provider")));
                       if (c.getInt(c.getColumnIndex(FavoriteSongsProvider.FAVORITE)) != 1)
                            if (c.getInt(c.getColumnIndex(FavoriteSongsProvider.COUNT)) < 2) {
                               ContentValues values = new ContentValues();
                                values.put(FavoriteSongsProvider.COUNT, c.getInt(c.getColumnIndex(FavoriteSongsProvider.COUNT)) + 1);
                               getActivity().getContentResolver().update(FavoriteSongsProvider.CONTENT_URI, values, FavoriteSongsProvider.ID_PROVIDER + "= " + mBaiHat.get(position).getmStt(), null);//     Log.d("ID",c.getString(c.getColumnIndex(FavoriteSongsProvider.COUNT))+"//"+c.getString(c.getColumnIndex(FavoriteSongsProvider.FAVORITE)));
                            } else {
                                if (c.getInt(c.getColumnIndex(FavoriteSongsProvider.COUNT)) == 2) {
                                    ContentValues values = new ContentValues();
                                    values.put(FavoriteSongsProvider.COUNT, 0);
                                    values.put(FavoriteSongsProvider.FAVORITE, 2);
                                    getActivity().getContentResolver().update(FavoriteSongsProvider.CONTENT_URI, values, FavoriteSongsProvider.ID_PROVIDER + "= " +  mBaiHat.get(position).getmStt(), null);
                                    //  Log.d("ID1", c.getString(c.getColumnIndex(FavoriteSongsProvider.COUNT)) + "//" + c.getString(c.getColumnIndex(FavoriteSongsProvider.FAVORITE)));
                                }
                            }

                    } while (c.moveToNext());

              }

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fragmentMediaPlaybackContainer.animate().setInterpolator(new OvershootInterpolator(1)).translationY(viewTranslationY);      // giá trị viewTranslationY sau khi đã trừ statusbar va toolbar
                        }
                    });
                }
                if (positionActive != -1) {
                    positionActive = position;
                    allSongView.updateMediaFragment(mBaiHat.get(position));
                } else {
                    positionActive = position;
                    allSongView.showMediaPlaybackPlayer(mBaiHat.get(position), position);
                }
                allSongView.onItemMusicClickListener(mBaiHat.get(position));
                MyNotification.createNotification(getContext(),mBaiHat.get(position),true);


                // luu so lan click 1 bai hat vao day
               // query database bai hat theo id
                // lấy ra count
                // nếu >3 update is_favorite =2
                //<3 thì update lại count

            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        listMusicRecycleView.setLayoutManager(layoutManager);
        listMusicRecycleView.setItemViewCacheSize(100);     // luu lai cache cua cac item view ow day la 100, de cuon thi holder class ko phai goi lai -> muot
        listMusicRecycleView.setAdapter(listMusicRecycleViewAdapter);
        if (positionActive != -1) {
            allSongView.showMediaPlaybackPlayer(mBaiHat.get(positionActive), positionActive);
        }
        return view;
    }
    private static final String MY_PREFS_NAME = "MyPrefsFile";
    // hàm lấy chiều cao của thanh status bar trả về giá trị dp  (thanh chứa cột sóng, thời gian)
    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return LayoutController.pxToDp(result);
    }

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    private boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    private void showDialog(final String msg, final Context context,
                            final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{permission},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do your stuff
            } else {
                Toast.makeText(getContext(), "GET_ACCOUNTS Denied",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions,
                    grantResults);
        }
    }

    @Override
    public void onTouchToOpen() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fragmentMediaPlaybackContainer.animate().setInterpolator(new OvershootInterpolator(1)).translationY(0);
            }
        });
    }

    @Override
    public void onCollapseView() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fragmentMediaPlaybackContainer.animate().setInterpolator(new OvershootInterpolator(1)).translationY(screenHeight - LayoutController.dpToPx(60 + 55 + getStatusBarHeight()));
            }
        });
    }

    @Override
    public void updateListMusic(ArrayList<BaiHat> listMusic) {

    }

    @Override
    public void setMusicActive(ArrayList<BaiHat> listMusic, BaiHat baiHat, final int pos) {
        ArrayList<BaiHat> tmp = new ArrayList<>(mBaiHat);
        positionActive = pos;
        mBaiHat.clear();
        mBaiHat.addAll(tmp);
        listMusicRecycleViewAdapter.positionActive= positionActive;
        listMusicRecycleViewAdapter.notifyDataSetChanged();
    }


}