package huyencool.com.music2.view.new_interface;

import huyencool.com.music2.model.BaiHat;

public interface AllSongView {
    void onItemMusicClickListener(BaiHat baiHat);
    void showMediaPlaybackPlayer(BaiHat baiHat, int pos);
    void setViewPlayerListener(ViewPlayerListener viewPlayerListener);
    void updateMediaFragment(BaiHat baiHat);
}
