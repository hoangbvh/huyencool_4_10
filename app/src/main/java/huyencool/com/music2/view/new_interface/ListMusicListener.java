package huyencool.com.music2.view.new_interface;

import java.util.ArrayList;

import huyencool.com.music2.model.BaiHat;

public interface ListMusicListener {
    void setListMusic(ArrayList<BaiHat> list);
}
