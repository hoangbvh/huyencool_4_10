package huyencool.com.music2.view;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import huyencool.com.music2.Library;
import huyencool.com.music2.MyNotification;
import huyencool.com.music2.R;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.model.FavoriteSongsProvider;
import huyencool.com.music2.view.new_interface.CallBackNotification;
import huyencool.com.music2.view.new_interface.MusicPlayListener;
import huyencool.com.music2.view.new_interface.ViewPlayerListener;



public class MediaPlaybackFragment extends Fragment implements CallBackNotification {
    BaiHat baiHatActive;
    ArrayList<BaiHat> listBaiHat;
    TextView nameMusic;
    TextView artist;
    ImageView smallImageMusic;
    ImageView bigImageMusic;
    ImageView nextMusicButton;
    ImageView backToListButton;
    SeekBar seekBar;
    TextView timeBatdau;
    TextView timeKetthuc;
    ImageView pausePlayButtom;
    ImageView playButton;       //button play in header
    ImageView prevButton;
    ImageView likeButton;
    ImageView disLikeButton;
    ImageView moreButton;
    ImageView repeatButton;
    ImageView shuffleButton;
    int sTime = 0;
    long eTime = 0;
    boolean isPlaying = false;
    int positionActive = 0;
    int repeatType = 0;     //0 la ko lap, 1 la 1 bai, 2 la all
    Boolean isShuffle = false;
    //
    Boolean isFavorite = false;
    Boolean isDisLike = false;
    LinearLayout viewPlayButton;
    LinearLayout viewBackAndMoreButton;
    //
    ConstraintLayout headerMediaPlayer;
    ViewPlayerListener viewPlayerListener;

    //
    MusicPlayListener musicPlayListener;




    public void setListBaiHat(ArrayList<BaiHat> list){
        this.listBaiHat = list;
    }
    public void setPositionActive(int positionActive){
        this.positionActive = positionActive;
    }
    public void setMusicPlayListener(MusicPlayListener musicPlayListener){
        this.musicPlayListener = musicPlayListener;
    }

    public void setViewPlayerListener(ViewPlayerListener viewPlayerListener){
        this.viewPlayerListener = viewPlayerListener;
    }
    public MediaPlaybackFragment(){

    }
    public void setBaiHatActive(BaiHat baiHatActive){
        this.baiHatActive = baiHatActive;
    }


    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
             sTime = Integer.parseInt(intent.getStringExtra("currentTime"));
             isPlaying = intent.getBooleanExtra("isPlaying",false);

            if(!isPlaying){
                playButton.setImageResource(R.drawable.ic_play_black_round);
                pausePlayButtom.setImageResource(R.drawable.ic_play_black_round);
            }
            else{
                playButton.setImageResource(R.drawable.ic_pause_black_large);
                pausePlayButtom.setImageResource(R.drawable.ic_pause_black_large);
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    seekBar.setProgress(sTime/1000);
                    long mStart = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
                    long sStart = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (mStart * 60);
                    timeBatdau.setText(String.format("%d : %d", mStart, sStart));
                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(bReceiver, new IntentFilter("message")); // dki vs service
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(bReceiver); // huy dki service
    }



    private void changeSeekBar(int s){
      //  Log.d("sad",s +"");
        if (!isPlaying)
            return;
        seekBar.setProgress(s / 1000); // hien thi time chay
        if (sTime/1000 >= eTime/1000) {
            sTime = 0;
            if (repeatType == 1){       // nếu click lặp bài hát hiện tại
                changeSeekBar(0);
                musicPlayListener.seekTo(baiHatActive,1000);
            }
            else{

                if(repeatType == 2){       // truong hop lap lai tat ca
                    positionActive = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                    if (isShuffle){     // neu dang duoc đặt là xáo trộn bài hát (ngẫu nhiên)   thì random position
                        final int min = 0;
                        final int max = listBaiHat.size() - 1;
                        final int randomPosition = new Random().nextInt((max - min) + 1) + min;
                        positionActive = randomPosition;
                    }

                    updateUI(listBaiHat.get(positionActive));
                    musicPlayListener.onNext(positionActive);
                }
                else{           // truong hop ko lap la thi kiểm tra nếu bài hát đang hát ở cuối danh sách thì ko phát lại nữa
                    if (positionActive == listBaiHat.size() -1){
                        return;
                    }
                    positionActive = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                    if (isShuffle){     // neu dang duoc đặt là xáo trộn bài hát (ngẫu nhiên)   thì random position
                        final int min = 0;
                        final int max = listBaiHat.size() - 1;
                        final int randomPosition = new Random().nextInt((max - min) + 1) + min;
                        positionActive = randomPosition;
                    }
                    updateUI(listBaiHat.get(positionActive));
                    musicPlayListener.onNext(positionActive);
                }
                changeSeekBar(0);
            }
        }
        else{
            sTime = s + 1000;
            // gán lại thời gian của bài hát đang hát bi dung
            new Handler().postDelayed(new Runnable() { // hàm chơ 1 giây sau thực hiện tiếp
                @Override
                public void run() {
                    final long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
                    final long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
                    String phut="";
                    if(phutBatdau<10) phut = "0"+phutBatdau;
                    else phut = phutBatdau+"";
                    String giay="";
                    if(giayBatdau<10) giay = "0"+giayBatdau;
                    else giay = giayBatdau+"";
                    timeBatdau.setText(phut+":"+giay);
                    changeSeekBar(sTime);
                }
            },1000);
        }
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media_playback2, container, false);
         nameMusic = view.findViewById(R.id.musicName);
         artist = view.findViewById(R.id.musicArtist);
         smallImageMusic = view.findViewById(R.id.smallImageMusic);
         bigImageMusic = view.findViewById(R.id.musicBigImageView);
         seekBar = view.findViewById(R.id.seekBar);
         timeBatdau = view.findViewById(R.id.startTimeTextView);
         timeKetthuc = view.findViewById(R.id.endTimeTextView);
         pausePlayButtom = view.findViewById(R.id.pausePlayButton);
         nextMusicButton = view.findViewById(R.id.nextMusicButton);
         prevButton = view.findViewById(R.id.prevButton);
         likeButton = view.findViewById(R.id.likeButton);
         disLikeButton = view.findViewById(R.id.disLikeButton);
         moreButton = view.findViewById(R.id.moreButton);
         repeatButton = view.findViewById(R.id.repeatButton);
         shuffleButton = view.findViewById(R.id.shuffleButton);
         // header view
        viewBackAndMoreButton = view.findViewById(R.id.viewBackAndMoreButton);
        viewPlayButton = view.findViewById(R.id.viewPlayButton);

        if (musicPlayListener != null){
            musicPlayListener.onGetTime(baiHatActive);
        }
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            viewBackAndMoreButton.setVisibility(LinearLayout.GONE);
            viewPlayButton.setVisibility(View.VISIBLE);
        }
        else{
            viewBackAndMoreButton.setVisibility(LinearLayout.VISIBLE);
            viewPlayButton.setVisibility(View.GONE);
        }
        likeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                if (isFavorite){
                    values.put(FavoriteSongsProvider.ID_PROVIDER, baiHatActive.getmStt());
                    values.put(FavoriteSongsProvider.FAVORITE, 0);
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                }
                else{
                    values.put(FavoriteSongsProvider.ID_PROVIDER, baiHatActive.getmStt());
                    values.put(FavoriteSongsProvider.FAVORITE, 2);
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_selected);
                }
                if (isDisLike){

                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                }
                isFavorite = !isFavorite;
                isDisLike = !isDisLike;

                //getActivity().getContentResolver().update(FavoriteSongsProvider.CONTENT_URI, values, FavoriteSongsProvider.ID_PROVIDER + "= " + baiHatActive.getmStt(), null);
                //Toast.makeText(getContext(), "like song //" + baiHatActive.getmTenbaihat(), Toast.LENGTH_SHORT).show();
                   // luu lai danh sach bai hat vao bộ nhớ bài yêu thích và không yêu thích
                String x= "";
                String selection = FavoriteSongsProvider.ID_PROVIDER + "=" + positionActive;
                Cursor cursor  = getActivity().getContentResolver().query(FavoriteSongsProvider.CONTENT_URI, null, selection, null, null);
                if(cursor.moveToFirst()) {
                    x = cursor.getColumnIndex(FavoriteSongsProvider.ID) +"";    // lay ra gia tri id
                    //log.wtf
                }
                if (!x.equals("")){
                    getActivity().getContentResolver().update(FavoriteSongsProvider.CONTENT_URI, values, FavoriteSongsProvider.ID_PROVIDER + "= " + baiHatActive.getmStt(), null);
                    Log.d("tttt","gfddg");
                }
                else{
                    values.put(FavoriteSongsProvider.ID_PROVIDER, baiHatActive.getmStt());
                    getActivity().getContentResolver().insert(FavoriteSongsProvider.CONTENT_URI,values);
                    Toast.makeText(getContext(), "like song //" + baiHatActive.getmStt(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        disLikeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ContentValues values = new ContentValues();

                if (isDisLike){
                    values.put(FavoriteSongsProvider.FAVORITE, 0);
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                }
                else{
                    values.put(FavoriteSongsProvider.FAVORITE, 1);
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_selected);
                }
                if (isFavorite){
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                }
                isFavorite = !isFavorite;
                isDisLike = !isDisLike;

                getActivity().getContentResolver().update(FavoriteSongsProvider.CONTENT_URI, values, FavoriteSongsProvider.ID_PROVIDER + "= " + baiHatActive.getmStt(), null);
                Toast.makeText(getContext(), "dislike song //" + baiHatActive.getmTenbaihat(), Toast.LENGTH_SHORT).show();
                /* luu lai danh sach bai hat vao bộ nhớ bài yêu thích và không yêu thích
                String x = "";
                String selection = FavoriteSongsProvider.ID + "=" + positionActive;
                Cursor cursor  = getActivity().managedQuery(FavoriteSongsProvider.CONTENT_URI, null, selection, null, null);
                if(cursor.moveToFirst()) {
                    x = cursor.getColumnIndex(FavoriteSongsProvider.ID) +"";    // lay ra gia tri id
                    //log.wtf
                    Log.d("dklfa",x +"");
                }
                if (!x.equals("")){
                    //update                    x = cursor.getColumnIndex(FavoriteSongsProvider.ID) +"";    // lay ra gia tri id
                    //log.wtf
                    Log.d("dklfa",x +"");
                }
                if (!x.equals("")){
                    //update
                }
                else{
                    values.put(FavoriteSongsProvider.ID_PROVIDER, "chinh la id cua bai hat");
                    getActivity().getContentResolver().insert(FavoriteSongsProvider.CONTENT_URI,values);
                    Toast.makeText(getContext(), "like song //" + baiHatActive.getmStt(), Toast.LENGTH_SHORT).show();
                }*/
            }
        });



        moreButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getContext(),view);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.listFavorite:
                                return true;
                            case R.id.listDislike:
                            default:
                                return false;
                        }
                    }
                });
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.menu_media, popupMenu.getMenu());
                popupMenu.show();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                musicPlayListener.seekTo(baiHatActive,seekBar.getProgress() * 1000);
                sTime = seekBar.getProgress() * 1000;
                long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
                long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
                timeBatdau.setText(String.format("0%d:%d", phutBatdau, giayBatdau));

            }
        });
         //header media player
        headerMediaPlayer = view.findViewById(R.id.headerMediaPlayer);
        headerMediaPlayer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.VISIBLE);
                viewPlayButton.setVisibility(View.GONE);
                viewPlayerListener.onTouchToOpen();
            }
        });
        //
        backToListButton = view.findViewById(R.id.backToListButton);
        backToListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.GONE);
                viewPlayButton.setVisibility(View.VISIBLE);
                viewPlayerListener.onCollapseView();
            }
        });
        playButton = view.findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateState();

            }
        });

         pausePlayButtom.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 updateState();
             }
         });
         nextMusicButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 if (isShuffle){     // neu dang duoc đặt là xáo trộn bài hát (ngẫu nhiên)   thì random position
                     final int min = 0;
                     final int max = listBaiHat.size() - 1;
                     final int randomPosition = new Random().nextInt((max - min) + 1) + min;
                     positionActive = randomPosition;
                 }
                 else{
                     positionActive = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                 }
                 setIsPlayButtonImage();
                 updateUI(listBaiHat.get(positionActive));
                 musicPlayListener.onNext(positionActive);
                 MyNotification.update(getContext(),listBaiHat.get(positionActive), isPlaying);
             }
         });
        prevButton.setOnClickListener(new View.OnClickListener( ){
            @Override
            public void onClick(View view) {
                if (sTime > 3000){
                    musicPlayListener.seekTo(baiHatActive, 0);
                    seekBar.setProgress(0);
                    timeBatdau.setText(String.format("0%d:%d", 0, 0));
                }else{
                    positionActive = (positionActive == 0) ? listBaiHat.size() - 1 : positionActive - 1;

                }
                setIsPlayButtonImage();
                updateUI(listBaiHat.get(positionActive));
                MyNotification.update(getContext(),listBaiHat.get(positionActive), isPlaying);
                musicPlayListener.onPrev(positionActive);
            }
        });

        //click lap lai bai hat
        repeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (repeatType == 0){
                    repeatType = 1;
                }
                else if (repeatType == 1){
                    repeatType = 2;
                }
                else repeatType = 0;
                updateRepeatIcon(repeatType);
            }
        });
        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isShuffle = !isShuffle;
                updateShuffleIcon(isShuffle);

            }
        });
        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME,  Activity.MODE_PRIVATE);
        repeatType = prefs.getInt("repeatType", 0);     //default = 0
        isShuffle = prefs.getBoolean("isShuffle", false);   //default = false

        updateRepeatIcon(repeatType);
        updateShuffleIcon(isShuffle);

        if (baiHatActive != null){

            Log.d("kjddasd",isPlaying +"");
            MyNotification.update(getContext(),baiHatActive,isPlaying);
            updateUI(baiHatActive);

        }

        return view;
    }


    private static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void updateShuffleIcon(Boolean isShuffle){
        if (isShuffle){
            shuffleButton.setImageResource(R.drawable.ic_play_shuffle_orange);
        }
        else{
            shuffleButton.setImageResource(R.drawable.ic_play_shuffle_off);
        }
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, Activity.MODE_PRIVATE).edit();
        editor.putBoolean("isShuffle",isShuffle);
        editor.apply();
    }
    private void updateRepeatIcon(int repeatType){
        switch (repeatType){
            case 0: // ko lap lai
                repeatButton.setImageResource(R.drawable.ic_repeat_off);
                break;
            case 1: //lap lai 1 bai
                repeatButton.setImageResource(R.drawable.ic_repeat_one);
                break;
            case 2: //lap lai tat ca
                repeatButton.setImageResource(R.drawable.ic_repeat_all);
                break;
        }
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, Activity.MODE_PRIVATE).edit();
            editor.putInt("repeatType",repeatType);
        editor.apply();
    }
    private void updateState(){
        MyNotification.update(getContext(),baiHatActive,!isPlaying);
        if(isPlaying){
            musicPlayListener.onPause(baiHatActive);
            playButton.setImageResource(R.drawable.ic_play_black_round);
            pausePlayButtom.setImageResource(R.drawable.ic_play_black_round);
            isPlaying = false;
        }
        else{
            isPlaying = true;
            musicPlayListener.onPlay(baiHatActive);
            changeSeekBar(sTime);
            playButton.setImageResource(R.drawable.ic_pause_black_large);
            pausePlayButtom.setImageResource(R.drawable.ic_pause_black_large);
        }
    }
    public void updateUI(BaiHat baiHat){
        sTime = 0;
        eTime = baiHat.getmTime();
        baiHatActive = baiHat;
        seekBar.setMax((int)eTime/1000);
        setIsPlayButtonImage();
        positionActive = listBaiHat.indexOf(baiHat);
        viewPlayerListener.setMusicActive(listBaiHat,baiHat,listBaiHat.indexOf(baiHat)); // đặt lại bài hát đang chọn cho allsongfragment để in đậm tên bài, thay icon, gọi lại khi xoay ngang...
        smallImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
        nameMusic.setText(baiHat.getmTenbaihat());
        artist.setText(baiHat.getmTencasi());
        bigImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
        if (!isPlaying){
            isPlaying = true;
            changeSeekBar(sTime);
        }
        long minutesEnd = TimeUnit.MILLISECONDS.toMinutes(baiHat.getmTime());
        long secondsEnd = TimeUnit.MILLISECONDS.toSeconds(baiHat.getmTime()) - (minutesEnd * 60);
        timeKetthuc.setText(String.format("0%d:%d", minutesEnd, secondsEnd));
    }

    @Override
    public void callBack(BaiHat baiHat,int pos, String action) {
        positionActive = pos;
        viewPlayerListener.setMusicActive(listBaiHat,baiHat,positionActive); // đặt lại bài hát đang chọn cho allsongfragment để in đậm tên bài, thay icon, gọi lại khi xoay ngang...
        if(action != null){
            switch (action) {
                case "prev": {
                    setIsPlayButtonImage();
                    if (sTime > 3000){
                        musicPlayListener.seekTo(baiHatActive, 0);
                        seekBar.setProgress(0);
                        timeBatdau.setText(String.format("%d : %d", 0, 0));
                    }else {
                        positionActive = (positionActive == 0) ? listBaiHat.size() - 1 : positionActive - 1;
                    }
                    sTime = 0;
                    updateUI(listBaiHat.get(positionActive));
                    musicPlayListener.onPrev(positionActive);
                    isPlaying = true;
                    MyNotification.update(getContext(),listBaiHat.get(positionActive), isPlaying);
                    break;
                }
                case "next": {
                    setIsPlayButtonImage();
                    if (isShuffle){     // neu dang duoc đặt là xáo trộn bài hát (ngẫu nhiên)   thì random position
                        final int min = 0;
                        final int max = listBaiHat.size() - 1;
                        final int randomPosition = new Random().nextInt((max - min) + 1) + min;
                        positionActive = randomPosition;
                    }
                    else{
                        positionActive = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                    }
                    sTime = 0;
                    updateUI(listBaiHat.get(positionActive));
                    musicPlayListener.onNext(positionActive);
                    isPlaying = true;
                    MyNotification.update(getContext(),listBaiHat.get(positionActive), isPlaying);
                    break;
                }
                case "pause":
                    isPlaying = true;
                    updateState();
                    break;
                case "play":
                    isPlaying = false;
                    updateState();
                    break;
            }
        }
    }

    private void setIsPlayButtonImage(){
        playButton.setImageResource(R.drawable.ic_pause_black_large);
        pausePlayButtom.setImageResource(R.drawable.ic_pause_black_large);
    }
}
