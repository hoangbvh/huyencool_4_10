package huyencool.com.music2.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import huyencool.com.music2.R;
import huyencool.com.music2.controller.LandscapeController;
import huyencool.com.music2.controller.LayoutController;
import huyencool.com.music2.controller.PortraitController;

public class Main2Activity extends AppCompatActivity {

    Toolbar toolbar;
    private LayoutController layoutController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigationdrawer);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            layoutController = new PortraitController(this);
        }
        else
        {
            layoutController = new LandscapeController(this);
        }
        createNotificationChannel();
        layoutController.onCreate(savedInstanceState,"DsBaihat");
    }
    private static final String  CHANNEL_ID = "SYMPER_1";

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Music";
            String description = "MusicNotification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        layoutController.onSaveInstanceState(outState);
    }
    public boolean onCreateOptionsMenu(Menu menu) { //Hien thi nut search
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
}
