package huyencool.com.music2.view.new_interface;

import huyencool.com.music2.model.BaiHat;

public interface MusicPlayListener {
    void onPause(BaiHat baiHat);
    void onPlay(BaiHat baiHat);
    void start(int positionActive);
    void stop();
    void onNext(int positionActive);
    void onPrev(int positionActive);
    void onGetTime(BaiHat baiHat);
    void seekTo(BaiHat baiHat,int time);
}
