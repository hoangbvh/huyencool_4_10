package huyencool.com.music2.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class BaiHat implements Serializable {
    String mStt;
    String mTenbaihat;
    String mTencasi;
    long mTime;
    String uri;
    String anh;
    String id;

    public BaiHat()
    {
        super();
    }
    public BaiHat(String mStt, String mTenbaihat, String mTencasi, long mTime, String uri,String anh, String id)
    {
        super();
        this.mStt=mStt;
        this.mTenbaihat=mTenbaihat;
        this.mTencasi=mTencasi;
        this.mTime=mTime;
        this.uri=uri;
        this.anh=anh;
        this.id = id;
    }
    public String getmStt()
    {
        return mStt;
    }
    public String  getmTenbaihat()
    {
        return mTenbaihat;
    }
    public String getmTencasi()
    {
        return mTencasi;
    }

    public long getmTime()
    {
        return mTime;
    }
    public String getAnh()
    {
        return anh;
    }
    public String getUri()
    {
        return uri;
    }
    public String getId()
    {
        return id;
    }
}

