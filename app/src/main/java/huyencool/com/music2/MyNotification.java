package huyencool.com.music2;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import java.util.ArrayList;

import huyencool.com.music2.controller.LayoutController;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.view.new_interface.CallBackNotification;

public class
MyNotification extends Notification  implements Parcelable {
    private static NotificationManager nManager;
    private static RemoteViews remoteView;
    private static RemoteViews bigRemoteView;
    public static ArrayList<BaiHat> listBaiHat;
    public static CallBackNotification callBackNotification;
    private static int posActive = -1;
    @SuppressLint("NewApi")
    private static final String CHANNEL_ID = "SYMPER_1";
    private static NotificationCompat.Builder nBuilder;
    public static void update(Context context,BaiHat baiHat, Boolean isPlaying){
        initUI(baiHat,isPlaying);
        nBuilder.setCustomContentView(remoteView)
                .setCustomBigContentView(bigRemoteView);

        setListeners(remoteView, context, isPlaying);
        setListeners(bigRemoteView, context, isPlaying);
        nManager.notify(1, nBuilder.build());
    }
    public static void createNotification(Context context, BaiHat baiHat, Boolean isPlaying) {

        Intent activityIntent = new Intent(context, LayoutController.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, activityIntent, 0);

        remoteView = new RemoteViews(context.getPackageName(), R.layout.notifications);

        bigRemoteView = new RemoteViews(context.getPackageName(), R.layout.big_notification);
        initUI(baiHat,isPlaying);


        nBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_pause_black_large)
                .setAutoCancel(false)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(remoteView)
                .setCustomBigContentView(bigRemoteView)
                .setContentIntent(contentIntent);

        setListeners(remoteView, context, isPlaying);
        setListeners(bigRemoteView, context, isPlaying);
        nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(1, nBuilder.build());
    }

    private static void initUI(BaiHat baiHat, boolean isPlaying){
        remoteView.setImageViewBitmap(R.id.ImageMusic, Library.StringToBitMap(baiHat.getAnh()));
        remoteView.setImageViewResource(R.id.nextButtom, R.drawable.ten2);
        remoteView.setImageViewResource(R.id.prevButtom, R.drawable.ic_rew_dark);

        bigRemoteView.setTextViewText(R.id.baihat, baiHat.getmTenbaihat());
        bigRemoteView.setTextViewText(R.id.casi, baiHat.getmTencasi());
        bigRemoteView.setImageViewBitmap(R.id.ImageMusic,Library.StringToBitMap(baiHat.getAnh()));
        bigRemoteView.setImageViewResource(R.id.nextButtom, R.drawable.ten2);
        bigRemoteView.setImageViewResource(R.id.prevButtom, R.drawable.ic_rew_dark);
        if (isPlaying) {
            remoteView.setImageViewResource(R.id.pauseButtom, R.drawable.ic_media_pause_light);
            bigRemoteView.setImageViewResource(R.id.pauseButtom, R.drawable.ic_media_pause_light);
        } else {
            remoteView.setImageViewResource(R.id.pauseButtom, R.drawable.ic_media_play_light);
            bigRemoteView.setImageViewResource(R.id.pauseButtom, R.drawable.ic_media_play_light);
        }
        posActive = listBaiHat.indexOf(baiHat);


    }
    private static void setListeners(RemoteViews view, Context context, Boolean isPlaying) {
        CallBackNotificationBroadcast callBackNotificationBroadcast = new CallBackNotificationBroadcast();
        CallBackNotificationBroadcast.listBaiHat = listBaiHat;
        CallBackNotificationBroadcast.pos = posActive;
        CallBackNotificationBroadcast.callBackNotification = callBackNotification;

        Log.d("allvas",context +"");
        Intent prev = new Intent(context, callBackNotificationBroadcast.getClass());
        prev.setAction("prev");
        PendingIntent prevButton = PendingIntent.getBroadcast(context, 1, prev, PendingIntent.FLAG_CANCEL_CURRENT);
        view.setOnClickPendingIntent(R.id.prevButtom, prevButton);


        //listener 2
        Intent pause = new Intent(context, callBackNotificationBroadcast.getClass());
        String action = (isPlaying) ? "pause" : "play"; // No dang phat thi pause con  dang pause thi play
        pause.setAction(action);
        PendingIntent pauseButton = PendingIntent.getBroadcast(context, 1, pause, PendingIntent.FLAG_CANCEL_CURRENT);
        view.setOnClickPendingIntent(R.id.pauseButtom, pauseButton);


        //listener 2
        Intent next = new Intent(context, callBackNotificationBroadcast.getClass());
        next.setAction("next");
        PendingIntent nextButton = PendingIntent.getBroadcast(context, 1, next, PendingIntent.FLAG_CANCEL_CURRENT);
        view.setOnClickPendingIntent(R.id.nextButtom, nextButton);
    }



}































