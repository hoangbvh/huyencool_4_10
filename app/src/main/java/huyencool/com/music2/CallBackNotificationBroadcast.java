package huyencool.com.music2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.view.new_interface.CallBackNotification;

public class CallBackNotificationBroadcast extends BroadcastReceiver {
    public static  ArrayList<BaiHat> listBaiHat;
    public static int pos;
    public static  CallBackNotification callBackNotification;

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        callBackNotification.callBack(listBaiHat.get(pos),pos,action);
    }

}